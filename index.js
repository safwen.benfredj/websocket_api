require("dotenv").config();
const fs = require("fs");
const shell = require("shelljs");
const bodyParser = require("body-parser");
const ftxController = require("./controllers/ftx.controller");
const express = require("express");

const app = express();
const verifToken = require("./utils/verifToken");
const { stderr, stdout } = require("process");

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: false,
  })
);
app.post("/generatecode", verifToken, async (req, res) => {
  let { apikey, secret, subAccount } = req.body;

  try {
    const file = `  
FROM node:14

# Create app directory
WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .
ENV API_KEY ${apikey}
ENV SECRET  ${secret}
ENV SUB_ACCOUNT ${subAccount}
EXPOSE 8080
CMD [ "node", "index.js" ]


`;
    shell.exec("cd socket && rm Dockerfile");

    fs.writeFileSync("./socket/Dockerfile", file);
    shell.exec(
      `cd socket && docker build . -t socket${apikey.toLowerCase().trim()}`
    );
    shell.exec(
      `docker run -d --name ${apikey.toLowerCase().trim()} socket${apikey
        .toLowerCase()
        .trim()} `
    );
    shell.exec(`docker ps -aqf "name=${apikey.toLowerCase().trim()}"`, (code, stdout, stderr) => {
      console.log("STDOUT", stdout);
      res.json({
        success: true,
        socket: "started",
        containerId: stdout.trim()
      });
    })

  } catch (err) {
    console.log(err);
  }
});
app.post("/getuserdata", ftxController.fetchUserData);
app.post("/market/info", ftxController.getSingleMarket);
app.get("/containers", (req, res) => {
  try {
    shell.exec(
      `docker ps -a --format "{{json .}}" |  jq -n '.containers |= [inputs]'`,
      (code, stdout, stderr) => {
        console.log("Exit code:", code);
        console.log("Program output:", stdout);
        console.log("Program stderr:", stderr);
        res.json({
          success: true,
          result: JSON.parse(stdout),
        });
      }
    );
  } catch (error) {
    res.json({
      success: false,
      result: error.message,
    });
  }
});
app.post("/container/stop", (req, res) => {
  let { containerId } = req.body;
  try {
    shell.exec(
      `docker container stop ${containerId}`,
      (code, stdout, stderr) => {
        console.log("Exit code:", code);
        console.log("Program output:", stdout);
        console.log("Program stderr:", stderr);
        res.json({
          success: true,
          result: stdout,
        });
      }
    );
  } catch (error) {
    res.json({
      success: false,
      result: error.message,
    });
  }
});
app.post("/container/start", (req, res) => {
  let { containerId } = req.body;
  try {
    shell.exec(
      `docker container start ${containerId}`,
      (code, stdout, stderr) => {
        console.log("Exit code:", code);
        console.log("Program output:", stdout);
        console.log("Program stderr:", stderr);
        if (stderr) {
          res.json({
            success: false,
            result: stderr,
          });
        } else {
          res.json({
            success: true,
            result: stdout,
          });
        }

      }
    );
  } catch (error) {
    res.json({
      success: false,
      result: error.message,
    });
  }
});
app.delete('/container/delete/:id', (req, res) => {

  let { id } = req.params;
  try {
    shell.exec(
      `docker container stop ${id}`,
      (code, stdout, stderr) => {
        console.log("Exit code:", code);
        console.log("Program output:", stdout);
        console.log("Program stderr:", stderr);
        shell.exec(`docker rm ${id}`, (code, stdout, stderr) => {

          res.json({
            success: true,
            result: stdout,
          });
        })

      }
    );
  } catch (error) {
    res.json({
      success: false,
      result: error.message,
    });
  }
})

app.listen(3000, () => {
  console.log("running..");
});
