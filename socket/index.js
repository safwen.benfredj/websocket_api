const FTXWs = require("ftx-api-ws");
const axios = require("axios").default;

// including private channels:
const ftx = new FTXWs({
  key: process.env.API_KEY,
  secret: process.env.SECRET,
  subaccount: process.env.SUB_ACCOUNT
});

const go = async () => {
  await ftx.connect();
  console.log("connectedd..");
  // if you passed api credentials:
  ftx.subscribe("orders");
  ftx.on("orders", (data) => {
    console.log(data);
    if (data.status == "closed") {
      let dataToSend = {
        key: process.env.API_KEY,
        order: data
      };
      let ind = Math.floor(Math.random() * (2 - 0 + 1)) + 0;
      axios
        .post(
          "https://hook.integromat.com/yyft6qa2jb6ltvuexyxhe1fykg454l13",
          dataToSend
        )
        .then()
        .catch((err) => console.log(err));
    }
  });

  // if you want to know when the status of underlying socket changes
  ftx.on("statusChange", console.log);
};

go();
