const ftx = require("ftx-api");

const fetchUserData = async (req, res) => {
  try {
    let { apikey, secret, subAccount } = req.body;
    let client = new ftx.RestClient(apikey, secret, {
      subAccountName: subAccount,
    });
    let result = await client.getAccount();

    res.json({
      success: true,
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      result: error.message,
    });
  }
};
const getSingleMarket = async (req, res) => {

  try {
    let { apikey, secret, subAccount, market } = req.body;
    console.log(req.body);
    let client;
    if (subAccount) {
      let account = subAccount;
      client = new ftx.RestClient(apikey, secret, {
        subAccountName: subAccount,
        externalReferralProgram: "compendiumfi",
      });
    } else {
      client = new ftx.RestClient(apikey, secret, {
        externalReferralProgram: "compendiumfi",
      });
    }
    let marketInfo = await client.getMarket(market); 
    console.log("Market Info",marketInfo) ; 
    res.json(marketInfo);
  } catch (error) {
    console.log(error)
    res.json({
      
      success: false,
      result: error.message,
    });
  }
}
module.exports = {
  fetchUserData,
  getSingleMarket
};
