const verifyToken = (req, res, next) => {
  var token = req.headers["x-access-token"];
  if (!token)
    return res.status(403).send({ auth: false, message: "No token provided." });
  if (token != process.env.API_KEY)
    return res.status(403).send({ auth: false, message: "Invalid Token." });
  next();
};

module.exports = verifyToken ; 